package cz.florian.bscassignment.di.modules

import cz.florian.bscassignment.screens.edit.EditModel
import cz.florian.bscassignment.screens.list.ListModel
import cz.florian.bscassignment.services.repositories.NotesRepository
import dagger.Module
import dagger.Provides

@Module
class ModelModule {

    @Provides
    fun getEditModel(repository: NotesRepository): EditModel = EditModel(repository)

    @Provides
    fun getListModel(repository: NotesRepository): ListModel = ListModel(repository)
}