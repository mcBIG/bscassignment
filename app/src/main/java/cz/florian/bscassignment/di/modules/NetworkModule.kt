package cz.florian.bscassignment.di.modules

import cz.florian.bscassignment.services.network.NetworkServiceFactory
import cz.florian.bscassignment.services.network.NotesEndpoint
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideNotesEndpoint(): NotesEndpoint = NetworkServiceFactory().createNotesEndpoint()
}