package cz.florian.bscassignment.di.components

import cz.florian.bscassignment.di.modules.AppModule
import cz.florian.bscassignment.di.modules.ModelModule
import cz.florian.bscassignment.di.modules.NetworkModule
import cz.florian.bscassignment.di.modules.StorageModule
import cz.florian.bscassignment.screens.edit.EditViewModel
import cz.florian.bscassignment.screens.list.ListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        StorageModule::class,
        ModelModule::class
    ]
)
interface AppComponent {
    fun inject(vm: ListViewModel)
    fun inject(vm: EditViewModel)
}