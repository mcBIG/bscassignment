package cz.florian.bscassignment.di.modules

import android.content.Context
import cz.florian.bscassignment.services.network.NotesEndpoint
import cz.florian.bscassignment.services.repositories.NotesRepository
import cz.florian.bscassignment.services.storage.AppDatabase
import cz.florian.bscassignment.services.storage.daos.NotesDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {
    @Provides
    @Singleton
    fun provideNotesDao(context: Context): NotesDao = AppDatabase.getDatabase(context).notesDao()

    @Provides
    @Singleton
    fun provideRepository(notesDao: NotesDao, notesEndpoint: NotesEndpoint): NotesRepository =
        NotesRepository(notesDao, notesEndpoint)
}