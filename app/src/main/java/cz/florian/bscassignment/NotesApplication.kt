package cz.florian.bscassignment

import android.app.Application
import cz.florian.bscassignment.di.components.AppComponent
import cz.florian.bscassignment.di.components.DaggerAppComponent
import cz.florian.bscassignment.di.modules.AppModule
import cz.florian.bscassignment.di.modules.ModelModule
import cz.florian.bscassignment.di.modules.NetworkModule
import cz.florian.bscassignment.di.modules.StorageModule

class NotesApplication : Application() {
    val appComponent: AppComponent by lazy { createAppComponent() }

    private fun createAppComponent(): AppComponent {
        return DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule())
            .storageModule(StorageModule())
            .modelModule(ModelModule())
            .build()
    }

}