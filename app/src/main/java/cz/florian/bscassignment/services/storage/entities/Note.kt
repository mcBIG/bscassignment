package cz.florian.bscassignment.services.storage.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
class Note(
    @PrimaryKey
    val id: String,
    val content: String,
    val remoteId: Int?
)