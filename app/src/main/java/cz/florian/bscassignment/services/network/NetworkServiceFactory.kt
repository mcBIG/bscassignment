package cz.florian.bscassignment.services.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkServiceFactory {

    companion object {
        private const val NOTES_ENDPOINT_BASE_URL = "https://private-b1d53-mcbig.apiary-mock.com/"
    }

    fun createNotesEndpoint(): NotesEndpoint {
        val httpClient = createHttpClient()
        val retrofit = createRetrofitFactory(httpClient)
        return retrofit.create(NotesEndpoint::class.java)
    }

    private fun createRetrofitFactory(httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NOTES_ENDPOINT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()
    }

    private fun createHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .build()
    }
}