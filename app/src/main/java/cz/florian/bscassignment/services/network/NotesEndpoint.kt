package cz.florian.bscassignment.services.network

import cz.florian.bscassignment.services.repositories.models.Note
import retrofit2.http.*

interface NotesEndpoint {
    @GET("/notes")
    suspend fun getNotes(): List<Note>

    @GET("/notes/{id}")
    suspend fun getNote(@Path("id") id: Int): Note

    @POST("/notes")
    suspend fun postNote(@Body notes: Note): Note

    @PUT("/notes/{id}")
    suspend fun updateNote(@Body title: Note): Note

    @DELETE("/notes/{id}")
    suspend fun deleteNote(@Path("id") id: Int)
}