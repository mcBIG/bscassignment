package cz.florian.bscassignment.services.repositories

import cz.florian.bscassignment.services.network.NotesEndpoint
import cz.florian.bscassignment.services.repositories.models.Note
import cz.florian.bscassignment.services.storage.daos.NotesDao

class NotesRepository(val notesDao: NotesDao, val notesEndpoint: NotesEndpoint) {
    suspend fun getAllNotes(): List<Note> = notesEndpoint
        .getNotes()
        .map { Note(it.id, it.title) }

}