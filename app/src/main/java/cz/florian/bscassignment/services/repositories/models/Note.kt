package cz.florian.bscassignment.services.repositories.models

class Note(val id: Int?, val title: String)