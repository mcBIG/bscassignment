package cz.florian.bscassignment.services.storage.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.florian.bscassignment.services.storage.entities.Note

@Dao
interface NotesDao {
    @Query("SELECT * FROM notes")
    fun getAllNotes(): List<Note>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(note: Note)


}