package cz.florian.bscassignment.screens.list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cz.florian.bscassignment.R
import cz.florian.bscassignment.services.repositories.models.Note

class NotesAdapter(
    private val context: Context,
    private var data: List<Note>
) : RecyclerView.Adapter<NotesViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun getItemViewType(position: Int): Int = R.layout.note_card

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val item = LayoutInflater.from(context).inflate(viewType, parent, false)
        return NotesViewHolder(item)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.title.text = data[position].title
    }
}