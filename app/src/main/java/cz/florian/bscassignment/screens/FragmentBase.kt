package cz.florian.bscassignment.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding
import cz.florian.bscassignment.NotesApplication
import cz.florian.bscassignment.di.components.AppComponent

abstract class FragmentBase<TViewBinding : ViewBinding> : Fragment(), LifecycleObserver {
    private var _viewBindingHolder: TViewBinding? = null

    protected val viewBinding
        get() = _viewBindingHolder
    protected val appComponent: AppComponent
        get() = (activity?.application as NotesApplication).appComponent

    protected abstract fun initViewModel()

    protected abstract fun inflate(inflater: LayoutInflater, container: ViewGroup?): TViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initViewModel()
        _viewBindingHolder = inflate(inflater, container)
        lifecycle.addObserver(this)
        return viewBinding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewBindingHolder = null
    }

}