package cz.florian.bscassignment.screens.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.florian.bscassignment.services.repositories.models.Note
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListViewModel : ViewModel() {
    @Inject
    lateinit var model: ListModel

    fun getAllNotes(): LiveData<List<Note>> {
        val result = MutableLiveData<List<Note>>()
        viewModelScope.launch {
            val notes = model.getAllNotes()
            result.postValue(notes)
        }
        return result
    }

    // TODO: Implement the ViewModel
}