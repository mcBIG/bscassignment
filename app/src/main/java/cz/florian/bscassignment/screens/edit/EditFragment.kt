package cz.florian.bscassignment.screens.edit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import cz.florian.bscassignment.databinding.EditFragmentBinding
import cz.florian.bscassignment.screens.FragmentBase

class EditFragment : FragmentBase<EditFragmentBinding>() {

    private lateinit var viewModel: EditViewModel

    override fun inflate(inflater: LayoutInflater, container: ViewGroup?): EditFragmentBinding {
        return EditFragmentBinding.inflate(inflater, container, false)
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this).get(EditViewModel::class.java)
        appComponent.inject(viewModel)
    }

}
