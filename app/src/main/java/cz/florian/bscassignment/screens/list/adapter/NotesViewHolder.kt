package cz.florian.bscassignment.screens.list.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cz.florian.bscassignment.R

class NotesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val card: View = itemView
    val title: TextView = itemView.findViewById(R.id.title)
}
