package cz.florian.bscassignment.screens.list

import cz.florian.bscassignment.services.repositories.NotesRepository

class ListModel(private val repository: NotesRepository) {
    suspend fun getAllNotes() = repository.getAllNotes()
}