package cz.florian.bscassignment.screens.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import cz.florian.bscassignment.databinding.ListFragmentBinding
import cz.florian.bscassignment.screens.FragmentBase
import cz.florian.bscassignment.screens.list.adapter.NotesAdapter

class ListFragment : FragmentBase<ListFragmentBinding>() {

    private lateinit var viewModel: ListViewModel

    override fun inflate(inflater: LayoutInflater, container: ViewGroup?): ListFragmentBinding {
        return ListFragmentBinding.inflate(inflater, container, false)
    }

    override fun initViewModel() {
        viewModel = ViewModelProvider(this).get(ListViewModel::class.java)
        appComponent.inject(viewModel)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initData() {
        viewModel.getAllNotes().observe(this) {
            viewBinding?.recyclerView?.adapter = NotesAdapter(context!!, it)
        }
    }
}
